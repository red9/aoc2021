//
// Created by red on 05/05/2022.
//

#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::vector;

void a() {
  int x0;
  int x1 = INT32_MAX;
  int incr = 0;
  while (cin >> x0) {
    if (x0 > x1) {
      incr++;
    }
    x1 = x0;
  }
  cout << "increased " << incr << " times" << std::endl;
}

int main() {
  vector<int> input;
  int x;
  int incr = 0;
  while (cin >> x) {
    input.push_back(x);
  }
  for(int i = 0; i < input.size() - 3; i++) {
    if (input[i] < input[i+3]) {
      incr++;
    }
  }
  cout << "increased " << incr << " times" << std::endl;
  return 0;
}
