//
// Created by red on 12/05/2022.
//

#include "common.h"
#include <iostream>
#include <vector>
#include <queue>
#include <string>


using std::string;
using std::vector;
using std::queue;
using std::cin, std::cout, std::endl;

int cost(const vector<int>&, int);

int main() {
  string s;
  vector<int> input;
  int sum = 0;
  while(getline(cin, s, ',')) {
    int x = stoi(s);
    input.push_back(x);
    sum += x;
  }
  sort(input, std::less{});
  print_vector(input);
}

int cost(const vector<int>& input, int target) {
  int cost_sum = 0;
  for(int i = 0; i < input.size(); i++) {
    cost_sum += abs(input.at(i) - target);
  }
  return cost_sum;
}
