//
// Created by red on 06/05/2022.
//

#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>

using std::string;
using std::cout, std::cin, std::endl;
using std::vector;

void print_board(vector<int>*);
void print_vector(vector<int>);
bool any_winning_or(vector<bool>*);
int board_score(vector<bool>*, vector<int>*);
void print_board_with_draw(vector<bool>*, vector<int>*);

int main() {
  string draw_raw;
  getline(cin, draw_raw);
  std::istringstream draw_raw_s;
  draw_raw_s.str(draw_raw);
  vector<int> draw;
  for (std::string s; std::getline(draw_raw_s, s, ',');) {
    draw.push_back(stoi(s));
  }
  cout << "\n";
  vector<vector<int>> boards;
  vector<int> board;
  int x;
  while(cin >> x) {
    board.push_back(x);
    if (board.size() == 25) {
      boards.push_back(board);
      board.clear();
    }
  }
  size_t earliest_win = draw.size() - 1;
  size_t earliest_win_board = 0;
  int earliest_win_score = 0;
  size_t latest_win = 0;
  size_t latest_win_board = 0;
  int latest_win_score = 0;
  for (int i = 0; i < boards.size(); i++) {
    auto board  = boards[i];
    auto drawn = new vector<bool>(25, false);
    for (int j = 0; j < draw.size() - 1; j++) {
      for (int k = 0; k < board.size(); k++) {
        if (draw[j] == board[k]) {
          drawn->at(k) = true;
        }
      }
      bool wins = any_winning_or(drawn);
      if(wins) {
        if (earliest_win > j) {
          earliest_win = j;
          earliest_win_board = i;
          earliest_win_score = draw[j] * board_score(drawn, &board);
          break;
        } else if (latest_win < j) {
          latest_win = j;
          latest_win_board = i;
          latest_win_score = draw[j] * board_score(drawn, &board);
        }
        break;
      }
    }
  }
  printf("earliest winning board: %d, on draw %d (%d), score %d\n", earliest_win_board, earliest_win, draw[earliest_win], earliest_win_score);
  printf("latest winning board: %d, on draw %d (%d), score %d\n", latest_win_board, latest_win, draw[latest_win], latest_win_score);

  return 0;
}

int board_score(vector<bool>* drawn, vector<int>* board) {
  int sum = 0;
  for(int i = 0; i < board->size(); i++) {
    if (!drawn->at(i)) {
      sum += board->at(i);
    }
  }
  return sum;
}

bool any_winning_or(vector<bool>* drawn) {
  for (int row = 0; row < 5; row++) {
    int count = 0;
    for (int col = 0; col < 5; col++) {
      int idx = row * 5 + col;
      if (!drawn->at(idx)) {
        break;
      }
      count++;
    }
    if (count == 5) {return true;}
  }
  for (int col = 0; col < 5; col++) {
    int count = 0;
    for (int row = 0; row < 5; row++) {
      int idx = row * 5 + col;
      if (!drawn->at(idx)) {
        break;
      }
      count++;
    }
    if (count == 5) {return true;}
  }
  return false;
}

void print_vector(vector<int> input) {
  printf("[");
  for (size_t i = 0; i < input.size(); i++) {
    printf("%d ", input[i]);
  }
  printf("]\n");
}

void print_board(vector<int>* board) {
  for(size_t i = 0; i < board->size(); i++) {
    cout << board->at(i);
    if ((i % 5 == 4)) {
      cout << '\n';
    } else {
      cout << ' ';
    }
  }
  cout << '\n';
}

void print_board_with_draw(vector<bool>* drawn, vector<int>* board) {
  for(size_t i = 0; i < board->size(); i++) {
    cout << board->at(i);
    if (drawn->at(i)) {
      cout << '*';
    } else {
      cout << ' ';
    }
    if ((i % 5 == 4)) {
      cout << '\n';
    } else {
      cout << ' ';
    }
  }
  cout << '\n';
}