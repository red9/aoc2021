//
// Created by red on 05/05/2022.
//

#include <string>
#include <iostream>
#include <vector>
#include <cstdio>

using std::cout, std::cin, std::endl;
using std::string;
using std::vector;

int main() {
  struct move { int x, y; };
  string dir;
  int mag;
  vector<struct move> input;
  while (cin >> dir >> mag) {
    int x = 0;
    int y = 0;
    if (dir == "forward") {
      x = mag;
    } else if (dir == "back") {
      x = -mag;
    } else if (dir == "down") {
      y = mag;
    } else if (dir == "up") {
      y = -mag;
    }
    struct move m = {x, y};
    input.push_back(m);
  }
  int x = 0, y = 0;
  for (size_t i = 0; i < input.size(); i++) {
    x += input[i].x;
    y += input[i].y;
  }
  std::printf("day 2 a position: %d, %d\n", x, y);

  x = 0;
  y = 0;
  int aim = 0;
  for (size_t i = 0; i < input.size(); i++) {
    x += input[i].x;
    y += input[i].x * aim;
    aim += input[i].y;
  }
  std::printf("day 2 b position: %d, %d\n", x, y);
  return 0;
}