#include <iostream>

using std::cout;
using std::cin;


int main() {
  int x;
  while (cin >> x) {
    cout << x << std::endl;
  }
  return 0;
}
