//
// Created by red on 13/05/2022.
//

#ifndef AOC2021_COMMON_H
#define AOC2021_COMMON_H

#include <vector>
#include <functional>

using std::vector;

void print_vector(vector<int>& input);

template<class T, class C>
void sort(vector<T>& v, C cmp = std::less{});

#endif //AOC2021_COMMON_H
