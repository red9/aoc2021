//
// Created by red on 06/05/2022.
//

#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::vector;
using std::cout, std::cin;

struct point {int x, y;};
struct line { struct point a, b;};
int main() {
  string s;
  vector<struct line> input;
  while(std::getline(cin, s)) {
    int x1, y1, x2, y2;
    sscanf(s.c_str(), "%d,%d -> %d,%d", &x1, &y1, &x2, &y2);
    struct point a = {x1, y1};
    struct point b = {x2, y2};
    struct line ab = {a, b};
    input.push_back(ab);
  }
  return 0;
}
