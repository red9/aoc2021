//
// Created by red on 05/05/2022.
//

#include "day3.h"
#include <string>
#include <vector>
#include <iostream>
#include <cstdio>

using std::vector;
using std::string;
using std::cin, std::cout;
using std::printf;

int powi(int, int);
int find_mcb(vector<int>, size_t);
int find_lcb(vector<int>, size_t);

int main() {
  vector<string> input;
  vector<int> input_int;
  vector<int> bit_count;
  string s;
  while (cin >> s) {
    input.push_back(s);
    int i = (int) stol(s, 0, 2);
    input_int.push_back(i);
  }
  for (size_t i = 0; i < s.length(); i++) {
    bit_count.push_back(0);
  }
  size_t width = bit_count.size();
  for (size_t i = 0; i < input.size(); i++) {
    for (size_t j = 0; j < width; j++) {
      char c = input[i][j];
      if (c == '1') {
        bit_count[j]++;
      }
    }
  }
  int gamma = 0, epsilon = 0;
  for (int i = 0; i < width; i++) {
    int x = powi(2, width - 1 - i);
    if (bit_count[i] > input.size() / 2) {
      gamma += x;
    } else {
      epsilon += x;
    }
  }
  printf("day 3 a: gamma and epsilon: %d, %d\n", gamma, epsilon);

  int mcb = find_mcb(input_int, width - 1);
  int lcb = find_lcb(input_int, width - 1);

  printf("day 3 b:\n  mcb: %d\n  lcb: %d\n  mcb*lcb: %d\n", mcb, lcb, mcb*lcb);

  return 0;
}

void print_vector(vector<int> input) {
  cout << '[';
  for (size_t i = 0; i < input.size(); i++) {
    cout << input[i];
    if (i < input.size() - 1) { cout << " "; }
  }
  cout << "]\n";
}

int find_mcb(vector<int> input, size_t msb) {
  int mask = 1 << msb;
  int count = 0;
  for (size_t i = 0; i < input.size(); i++) {
    int bit = input[i] & mask;
    if (bit != 0) { count++; }
  }
  int mcb = 0;
  if ((count > input.size() / 2) || ((count == input.size() / 2) && (input.size() % 2 == 0))) {
    mcb = mask;
  }
  vector<int> input_strip;
  for (size_t i = 0; i < input.size(); i++) {
    if ((input[i] & mask) == mcb) {
      input_strip.push_back(input[i]);
    }
  }
  if (input_strip.size() == 1) { return input_strip[0]; }
  else if (msb == 0) { printf("this shouldn't happen\n"); return 0;}
  else { return find_mcb(input_strip, msb - 1); }
}

int find_lcb(vector<int> input, size_t msb) {
  int mask = 1 << msb;
  int count = 0;
  for (size_t i = 0; i < input.size(); i++) {
    int bit = input[i] & mask;
    if (bit != 0) { count++; }
  }
  int lcb = mask;
  if ((count > input.size() / 2) || ((count == input.size() / 2) && (input.size() % 2 == 0))) {
    lcb = 0;
  }
  vector<int> input_strip;
  for (size_t i = 0; i < input.size(); i++) {
    if ((input[i] & mask) == lcb) {
      input_strip.push_back(input[i]);
    }
  }
  if (input_strip.size() == 1) { return input_strip[0]; }
  else if (msb == 0) { printf("this shouldn't happen\n"); return 0;}
  else { return find_lcb(input_strip, msb - 1); }
}

int powi (int b, int x) {
  int r = 1;
  while(x > 0) {
    r *= b;
    x--;
  }

  return r;
}