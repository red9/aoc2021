//
// Created by red on 13/05/2022.
//

#include "common.h"
#include <iostream>
#include <vector>

using std::vector;
using std::cout, std::cin;

void print_vector(vector<int>& input) {
  cout << '[';
  for (size_t i = 0; i < input.size(); i++) {
    cout << input[i];
    if (i < input.size() - 1) { cout << " "; }
  }
  cout << "]\n";
}


template<class T>
void swap(T& a, T& b) {
  T temp = a;
  a = b;
  b = temp;
}

template<class T, class C>
void partition(vector<T>& v, T pivot, size_t& lo, size_t& hi, C cmp) {
  size_t mid = lo;
  while (mid <= hi) {
    T& val = v.at(mid);
    if (cmp(val, pivot)) {
      swap(val, v.at(lo));
      lo++;
      mid++;
    } else if (cmp(pivot, val)) {
      swap(val, v.at(hi));
      hi--;
    } else {
      mid++;
    }
  }
}

template<class T, class C>
T mo3(vector<T>& v, size_t lo, size_t hi, C cmp) {
  T& v_lo = v.at(lo);
  T& v_hi = v.at(hi);
  T& v_mid = v.at((lo+hi)/2);
  if (cmp(v_mid, v_lo)) {
    swap(v_mid, v_lo);
  }
  if (cmp(v_hi, v_lo)) {
    swap(v_hi, v_lo);
  }
  if (cmp(v_hi, v_mid)) {
    swap(v_hi, v_mid);
  }
  return v_mid;
}

template<class T, class C>
void shellsort(vector<T>& v, size_t& lo, size_t& hi, C cmp);

template<class T, class C>
void sort(vector<T>& v, C cmp) {
  vector<size_t> stack; // intervals next to be sorted
  stack.push_back(v.size());
  stack.push_back(0);
  while(stack.size() > 0) {
    size_t lo = stack.back(); stack.pop_back();
    size_t hi = stack.back(); stack.pop_back();
    if (hi - lo <= 12) { shellsort(v, lo, hi, cmp); }
    else {
      T mid = mo3(v, lo, hi);
      // mo3 guarantees that the first and last elements will be less than / greater than the pivot
      // so we can consider those partitioned
      size_t m_lo = lo + 1;
      size_t m_hi = hi - 1;
      partition(v, m_lo, m_hi, cmp);
      stack.push_back(hi);
      stack.push_back(m_hi);
      stack.push_back(m_lo);
      stack.push_back(lo);
    }
  }
}

template<class T, class C>
void shellsort(vector<T>& v, size_t& lo, size_t& hi, C cmp) {
  int gaps[8] = {701, 301, 132, 57, 23, 10, 4, 1};
  for (int gap : gaps) {
    for (int offset = 0; offset < gap; offset++) {
      for (size_t i = lo + offset; i < hi; i += gap) {
        T temp = v.at(i);
        size_t j = i;
        for (; j >= gap + lo && cmp(temp, v.at(j - gap)); j -= gap) {
          v.at(j) = v.at(j - gap);
        }
        v.at(j) = temp;
      }
    }
  }
}